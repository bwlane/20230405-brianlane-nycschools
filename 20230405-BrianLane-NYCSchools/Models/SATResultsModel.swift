import SwiftUI

private typealias Mock = Constants.Mock

struct SATResultsModel: Codable, Hashable, Identifiable {
    var id: String { dbn }
    let dbn, schoolName, numOfSatTestTakers, satCriticalReadingAvgScore: String
    let satMathAvgScore, satWritingAvgScore: String

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case numOfSatTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}

extension SATResultsModel {
    static var mock: SATResultsModel = SATResultsModel(
        dbn: Mock.DBN,
        schoolName: Mock.SchoolName,
        numOfSatTestTakers: Mock.TestTakers,
        satCriticalReadingAvgScore: Mock.ReadingScore,
        satMathAvgScore: Mock.MathScore,
        satWritingAvgScore: Mock.WritingScore
    )
}
