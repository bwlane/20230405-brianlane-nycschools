import SwiftUI

private typealias Mock = Constants.Mock

struct SchoolModel: Hashable, Codable, Identifiable {
    var id: String { dbn }
    var dbn, schoolName, boro: String

    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case boro
    }
}

extension SchoolModel {
    static var mock: SchoolModel = SchoolModel(
        dbn: Mock.DBN,
        schoolName: Mock.SchoolName,
        boro: Mock.Boro
    )
}


