//
//  _0230405_BrianLane_NYCSchoolsApp.swift
//  20230405-BrianLane-NYCSchools
//
//  Created by Brian Lane on 4/4/23.
//

import SwiftUI

@main
struct _0230405_BrianLane_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(
                vm: SchoolsListViewModel()
            )
                
        }
    }
}
