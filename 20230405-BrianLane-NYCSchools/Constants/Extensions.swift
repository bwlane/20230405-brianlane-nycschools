//
//  Extensions.swift
//  20230405-BrianLane-NYCSchools
//
//  Created by Brian Lane on 4/6/23.
//

import Foundation
import IdentifiedCollections

// MARK: - Array
extension Array where Element: Identifiable {
    func toIdentified() -> IdentifiedArrayOf<Element> {
        IdentifiedArrayOf(uniqueElements: self)
    }
}

// MARK: - Optional
extension Optional where Wrapped == Array<SATResultsModel> {
    func unwrappedOrEmpty() -> Array<SATResultsModel> {
        switch self {
        case .some(let arr):
            return arr
        case .none:
            return Array()
        }
    }
}

extension Optional where Wrapped == Array<SchoolModel> {
    func unwrappedOrEmpty() -> Array<SchoolModel> {
        switch self {
        case .some(let arr):
            return arr
        case .none:
            return Array()
        }
    }
}

// MARK: - Result
extension Result {
    func ok() -> Optional<Success> {
        switch self {
        case .success(let success):
            return .some(success)
        case .failure:
            return .none
        }
    }
}
