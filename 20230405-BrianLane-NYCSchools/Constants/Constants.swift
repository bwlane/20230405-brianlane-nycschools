import Foundation

struct Constants {
    
    struct URL {
        static let SchoolURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        static let SATURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    }
    
    struct Mock {
        static var DBN: String = "DEAD-BEEF"
        static var SchoolName: String = "Hale Kula Elementary School"
        static var TestTakers: String = "100"
        static var ReadingScore: String = "500"
        static var MathScore: String = "500"
        static var WritingScore: String = "500"
        static var Boro: String = "X"
    }
    
    struct Icons {
        static let Cap: String = "graduationcap.circle"
        static let Student: String = "studentdesk"
        static let Reading: String = "book"
        static let Math: String = "plus"
        static let Writing: String = "pencil"
        static let Hourglass: String = "hourglass.circle"
    }
    
    struct SAT {
        static let TestTakers: String = "Number of SAT Test Takers"
        static let ReadingScore: String = "SAT Critical Reading Average Score"
        static let MathScore: String = "SAT Math Average Score"
        static let WritingScore: String = "SAT Writing Average Score"
        static let NoInfoAvailable: String = "No SAT information available for this school."
    }
    
    struct Schools {
        static let NYC: String = "NYC Schools"
    }
}
