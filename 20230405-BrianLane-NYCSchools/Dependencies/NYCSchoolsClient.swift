import SwiftUI


enum SchoolError: Error {
    case basic(String)
}

class NYCSchoolsClient: ObservableObject {
    let fetchSchools: () async -> Result<[SchoolModel],SchoolError>
    let fetchSATResults: () async -> Result<[SATResultsModel], SchoolError>
    
    init(
        fetchSchools: @escaping () async -> Result<[SchoolModel],SchoolError>,
        fetchSATResults: @escaping () async -> Result<[SATResultsModel], SchoolError>
    ) {
        self.fetchSchools = fetchSchools
        self.fetchSATResults = fetchSATResults
    }
}

// MARK: - Live Value
extension NYCSchoolsClient {
    static let liveValue: NYCSchoolsClient = NYCSchoolsClient(
        fetchSchools: {
            do {
                let url = URL(string: Constants.URL.SchoolURL)!
                let (data, _) = try await URLSession.shared.data(from: url)
                let schools = try  JSONDecoder().decode([SchoolModel].self, from: data)
                return .success(schools)
            } catch {
                return .failure(.basic(error.localizedDescription))
            }
            
            
        },
        fetchSATResults: {
            do {
                let url = URL(string: Constants.URL.SATURL)!
                let (data, _) = try await URLSession.shared.data(from: url)
                let sats = try  JSONDecoder().decode([SATResultsModel].self, from: data)
                return .success(sats)
            } catch {
                return .failure(.basic(error.localizedDescription))
            }
        }
    )
}

// MARK: - Mock Value
extension NYCSchoolsClient {
    static var mock: NYCSchoolsClient = NYCSchoolsClient(
        fetchSchools: {
            return .success([.mock])
        },
        fetchSATResults: {
            return .success([.mock])
        }
    )
}

