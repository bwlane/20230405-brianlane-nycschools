//
//  SchoolDetailView.swift
//  20230405-BrianLane-NYCSchools
//
//  Created by Brian Lane on 4/4/23.
//

import SwiftUI

struct SchoolDetailView: View {
    var vm: SchoolDetailViewModel
    var body: some View {
        VStack {
            Image(systemName: Constants.Icons.Cap)
                .font(.system(size: 100))
            VStack(alignment: .leading) {
                Text(vm.school.schoolName)
                    .font(.system(.largeTitle))
                Divider()
                if let sat = vm.sat {
                    HStack {
                        Image(systemName: Constants.Icons.Student)
                        Text(Constants.SAT.TestTakers)
                        .font(.system(.title3))
                        .underline()
                    }

                    Text(sat.numOfSatTestTakers)
                        .font(.system(.title3))
                    HStack {
                        Image(systemName: Constants.Icons.Reading)
                        Text(Constants.SAT.ReadingScore)
                            .font(.system(.title3))
                            .underline()
                    }

                    Text(sat.satCriticalReadingAvgScore)
                        .font(.system(.title3))
                    HStack {
                        Image(systemName: Constants.Icons.Math)
                        Text(Constants.SAT.MathScore)
                            .font(.system(.title3))
                            .underline()
                    }
                    Text(sat.satMathAvgScore)
                        .font(.system(.title3))
                    HStack {
                        Image(systemName: Constants.Icons.Writing)
                        Text(Constants.SAT.WritingScore)
                            .font(.system(.title3))
                            .underline()
                    }

                    Text(sat.satWritingAvgScore)
                        .font(.system(.title3))
                } else {
                    Text(Constants.SAT.NoInfoAvailable)
                        .font(.system(.title3))
                        .underline()
                }
            }
            .padding()
        }
    }
}

struct SchoolDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SchoolDetailView(vm: .mock)
    }
    
}
