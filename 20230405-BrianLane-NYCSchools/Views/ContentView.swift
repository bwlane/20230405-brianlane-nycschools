//
//  ContentView.swift
//  20230405-BrianLane-NYCSchools
//
//  Created by Brian Lane on 4/4/23.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var vm: SchoolsListViewModel
    @State private var isRotating = 0.0

    var body: some View {
        NavigationView {
            if vm.schools.count == 0 {
                Image(systemName: Constants.Icons.Hourglass)
                    .font(.system(size: 64))
                    .rotationEffect(.degrees(isRotating))
                    .onAppear {
                        withAnimation(.linear(duration: 1)
                                .speed(0.1).repeatForever(autoreverses: false)) {
                                    isRotating = 360.0
                                }
                    }
            } else {
                List(vm.schools, id: \.self, selection: $vm.selectedSchool) { school in
                    Text(school.schoolName)
                }
                .navigationTitle(Constants.Schools.NYC)
            }

        }

        .sheet(item: $vm.selectedSchool) { school in
            SchoolDetailView(
                vm: SchoolDetailViewModel(
                    school: school,
                    sat: vm.satResults[id: school.dbn] ?? nil
                )
            )
        }
        .task {
            vm.schools = await vm.fetchSchools()
            vm.satResults = await vm.fetchSatResults()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(vm: .mock)
    }
}








