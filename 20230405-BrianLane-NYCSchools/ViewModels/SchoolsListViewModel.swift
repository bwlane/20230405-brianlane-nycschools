//
//  SchoolsListViewModel.swift
//  20230405-BrianLane-NYCSchools
//
//  Created by Brian Lane on 4/4/23.
//

import SwiftUI
import IdentifiedCollections

class SchoolsListViewModel: ObservableObject {
    @Published var schools: IdentifiedArrayOf<SchoolModel> = []
    @Published var satResults:IdentifiedArrayOf<SATResultsModel> = []
    @Published var selectedSchool: SchoolModel?
    private var schoolClient: NYCSchoolsClient
    
    init(_ schoolClient: NYCSchoolsClient = .liveValue) {
        self.schoolClient = schoolClient
    }
    
    func fetchSchools() async -> IdentifiedArrayOf<SchoolModel> {
        return await schoolClient
            .fetchSchools()
            .ok()
            .unwrappedOrEmpty()
            .toIdentified()
    }
    
    func fetchSatResults() async -> IdentifiedArrayOf<SATResultsModel> {
        return await schoolClient
            .fetchSATResults()
            .ok()
            .unwrappedOrEmpty()
            .toIdentified()
    }
}

extension SchoolsListViewModel {
    static var mock: SchoolsListViewModel {
        SchoolsListViewModel()
    }
}
