import SwiftUI

class SchoolDetailViewModel: ObservableObject {
    var school: SchoolModel
    var sat: SATResultsModel?
    
    init(
        school: SchoolModel,
        sat: SATResultsModel?
    ) {
        self.school = school
        self.sat = sat
    }
}

extension SchoolDetailViewModel {
    static var mock = SchoolDetailViewModel(
        school: .mock,
        sat: .mock
    )
}
