import XCTest
@testable import _0230405_BrianLane_NYCSchools

final class SchoolsListViewModelTests: XCTestCase {
    
    var sut: SchoolsListViewModel!
    
    func test_whenSchoolsAreFetched_schoolsArrayCountMatches() async {
        sut = SchoolsListViewModel(.mock)
        XCTAssert(sut.schools.count == 0)
        sut.schools = await sut.fetchSchools()
        XCTAssert(sut.schools.count == 1)
    }
    
    func test_whenSchoolsAreFetched_schoolsArrayContainsSchool() async {
        sut = SchoolsListViewModel(.mock)
        XCTAssert(sut.schools.count == 0)
        sut.schools = await sut.fetchSchools()
        XCTAssertNotNil(sut.schools.first)
        XCTAssert(sut.schools.first!.schoolName == Constants.Mock.SchoolName)
        XCTAssert(sut.schools.first!.dbn == Constants.Mock.DBN)
        XCTAssert(sut.schools.first!.boro == Constants.Mock.Boro)
    }
    
    func test_whenSATsAreFetched_SATsArrayCountMatches() async {
        sut = SchoolsListViewModel(.mock)
        XCTAssert(sut.satResults.count == 0)
        sut.satResults = await sut.fetchSatResults()
        XCTAssert(sut.satResults.count == 1)
    }
    
    func test_whenSATsAreFetched_SATsArrayContainsSchool() async {
        sut = SchoolsListViewModel(.mock)
        XCTAssert(sut.satResults.count == 0)
        sut.satResults = await sut.fetchSatResults()
        XCTAssertNotNil(sut.satResults.first)
        XCTAssert(sut.satResults.first!.schoolName == Constants.Mock.SchoolName)
        XCTAssert(sut.satResults.first!.dbn == Constants.Mock.DBN)
        XCTAssert(sut.satResults.first!.numOfSatTestTakers == Constants.Mock.TestTakers)
        XCTAssert(sut.satResults.first!.satMathAvgScore == Constants.Mock.MathScore)
        XCTAssert(sut.satResults.first!.satCriticalReadingAvgScore == Constants.Mock.ReadingScore)
        XCTAssert(sut.satResults.first!.satWritingAvgScore == Constants.Mock.WritingScore)
    }
    
    func test_whenSchoolsAndSATsAreFetched_SchoolAndSATMatch() async {
        sut = SchoolsListViewModel(.mock)
        XCTAssert(sut.satResults.count == 0)
        sut.schools = await sut.fetchSchools()
        sut.satResults = await sut.fetchSatResults()
        XCTAssert(sut.schools.count == 1)
        XCTAssert(sut.satResults.count == 1)
        let school = sut.schools[id: Constants.Mock.DBN]
        let sat = sut.satResults[id: Constants.Mock.DBN]
        XCTAssert(school!.dbn == sat!.dbn)
    }
    
    func test_whenSchoolsAndSATsAreFetched_SchoolHasNoSAT() async {
        NYCSchoolsClient.mock = NYCSchoolsClient(
            fetchSchools: {
                return .success([.mock])
            }, fetchSATResults: {
                return .success([
                    SATResultsModel(
                        dbn: "DEAD-FEEB",
                        schoolName: Constants.Mock.SchoolName,
                        numOfSatTestTakers: Constants.Mock.TestTakers,
                        satCriticalReadingAvgScore: Constants.Mock.ReadingScore,
                        satMathAvgScore: Constants.Mock.MathScore,
                        satWritingAvgScore: Constants.Mock.WritingScore
                    )
                ])
            })
        sut = SchoolsListViewModel(.mock)
        XCTAssert(sut.schools.count == 0)
        XCTAssert(sut.satResults.count == 0)
        sut.schools = await sut.fetchSchools()
        sut.satResults = await sut.fetchSatResults()
        XCTAssert(sut.schools.count == 1)
        XCTAssert(sut.satResults.count == 1)
        let school = sut.schools[id: Constants.Mock.DBN]
        let sat = sut.satResults.first!
        XCTAssert(school!.dbn != sat.dbn)
    }
}
